plugin.tx_ganttceis {
	view {
		templateRootPaths.0 = {$plugin.tx_ganttceis.view.templateRootPaths.0}
		partialRootPaths.0 = {$plugin.tx_ganttceis.view.partialRootPaths.0}
		layoutRootPaths.0 = {$plugin.tx_ganttceis.view.layoutRootPaths.0}
	}
	#By default the following settings only will have relevance if you have fluidcontent_core extension loaded
	settings{
		container {
			types {
				default = div
				Example = div
			}
		}
	}
}

page.includeCSS.estiloPag = EXT:gantt_ceis/Resources/Public/Css/disenoPagina.css
page.includeCSS.estiloGantt = EXT:gantt_ceis/Resources/Public/Css/disenoGantt.css
page.includeJSFooter.jqueryGantt = EXT:gantt_ceis/Resources/Public/Js/jquery.fn.gantt.js
page.includeJSFooter.gantt = EXT:gantt_ceis/Resources/Public/Js/configuracionGantt.js

#page.includeJSFooter.lib_filesaver = EXT:gantt_ceis/Resources/Public/Js/filesaver.js
#page.includeJSFooter.lib_html2canvas = EXT:gantt_ceis/Resources/Public/Js/html2canvas.js
#page.includeJSFooter.imp_img = EXT:gantt_ceis/Resources/Public/Js/imprimirImagen.js
#page.includeJSFooter.canvas2Image = EXT:gantt_ceis/Resources/Public/Js/canvas2image.js

