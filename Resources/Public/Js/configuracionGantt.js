/**
 * Created by Javier on 16-05-16.
 */

function recibeJsonGantt() {
    var inputRecibeJsonGantt = $("#campoRecibeJsonGantt");
    if (inputRecibeJsonGantt.val() != undefined) {
        if(inputRecibeJsonGantt.val().length > 0) {
            return JSON.parse(inputRecibeJsonGantt.val());
        }else {
            return 0;
        }
    } else {
        return 0;
    }
}

if (recibeJsonGantt() != 0) {

    $(function() {
        "use strict";
        $(".gantt").gantt({
            source: recibeJsonGantt(),
            navigate: "scroll",
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            dow: ["D", "L", "M", "M", "J", "V", "S"],
            scale: $("#escalaDefault").val(),
            maxScale: $("#escalaMaxima").val(),
            minScale: $("#escalaMinima").val(),
            waitText: "Cargando...",
            itemsPerPage: 15,
            useCookie: true,
            scrollToToday: false,

            onItemClick: function(dataObj) {
                $("#celdaTarea").text(dataObj["titulo"]);
                $("#celdaDescripcion").text(dataObj["descripcion"]);
                $("#celdaFechaIn").text(dataObj["fecha inicio"]);
                $("#celdaFechaTer").text(dataObj["fecha termino"]);

                $("#modalTarea").modal();
            }
        });
    });
}




