<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}


\FluidTYPO3\Flux\Core::registerProviderExtensionKey('CeisUfro.GanttCeis', 'Content');

$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['servicioGantt'] = 'EXT:gantt_ceis/Classes/EID/ServicioDatosGantt.php';

