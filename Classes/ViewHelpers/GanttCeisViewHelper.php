<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 16-05-16
 * Time: 16:53
 */

namespace CeisUfro\GanttCeis\ViewHelpers;

use TYPO3\CMS\Fluid\ViewHelpers\Form\AbstractFormViewHelper;
use TYPO3\CMS\Fluid\View\StandaloneView;

class GanttCeisViewHelper extends AbstractFormViewHelper {

    /**
     * @param string $enlace url de servicio donde obtener los datos
     * @param array $param parametros opcionales de servicio
     * @param string $escalaDefault
     * @param string $escalaMinima
     * @param string $escalaMaxima
     * @return string
     */
    public function render($enlace, array $param = NULL, $escalaDefault = 'weeks', $escalaMinima = 'days', $escalaMaxima = 'months') {

        $directorioVista = PATH_site."typo3conf/ext/gantt_ceis/Resources/Private/Templates/VistaGantt/";
        $rutaCompletaVista = $directorioVista.'gantt.html';
        $vista = new StandaloneView();
        $vista->setFormat("html");
        $vista->setTemplatePathAndFilename($rutaCompletaVista);

        $default = "";
        $minima = "";
        $maxima = "";

        switch($escalaDefault) {
            case "Horas":
                $default = "hours";
                break;
            case "Dias":
                $default = "days";
                break;
            case "Semanas":
                $default = "weeks";
                break;
            case "Meses":
                $default = "months";
                break;
        }

        switch($escalaMinima) {
            case "Horas":
                $minima = "hours";
                break;
            case "Dias":
                $minima = "days";
                break;
            case "Semanas":
                $minima = "weeks";
                break;
            case "Meses":
                $minima = "months";
                break;
        }

        switch($escalaMaxima) {
            case "Horas":
                $maxima = "hours";
                break;
            case "Dias":
                $maxima = "days";
                break;
            case "Semanas":
                $maxima = "weeks";
                break;
            case "Meses":
                $maxima = "months";
                break;
        }

        if(empty($enlace)) {
            $mensaje = "<h3>No se ingreso enlace a servicio de datos </h3>";
        } else {
            $json = $param != NULL ? $this->wsRestRequest("POST", $enlace, $param) : file_get_contents($enlace);
            $vista->assign("json",$json);
            $conenidoJson = json_decode($json,true);
            $vista->assign("contenidoJson",$conenidoJson);
            $vista->assign("escalaDefault",$default);
            $vista->assign("escalaMinima",$minima);
            $vista->assign("escalaMaxima",$maxima);
        }

        $vista->assign('mensaje',$mensaje);

        return $vista->render();
    }

    /**
     * Function to call a JSON REST Service
     *
     * @param string $method HTTP method: "GET"(default), "POST", "PUT" or "DELETE"
     * @param string $url dirección servicio
     * @param array $query_params Optional. Associative array containing the variables to use in the request if "POST" or "PUT" method.
     * @return mixed
     */
    private function wsRestRequest($method = "GET", $url, $query_params = NULL) {

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $method = strtoupper($method);

        if($method == "POST") {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query_params));
        }elseif($method == "DELETE" || $method == "PUT") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            if($query_params !== NULL) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query_params));
            }
        }

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

}