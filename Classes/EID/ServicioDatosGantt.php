<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 15-03-2016
 * Time: 11:14
 */


if (!defined ('PATH_typo3conf')) die ('Access denied.');

\TYPO3\CMS\Frontend\Utility\EidUtility::initTCA();

$id = isset($HTTP_GET_VARS['id'])?$HTTP_GET_VARS['id']:0;
header('Content-Type: application/json');

$TSFE = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController', $GLOBALS['TYPO3_CONF_VARS'], $id, '0', 1);
$GLOBALS['TSFE'] = $TSFE;
$GLOBALS['TSFE']->initFEuser(); // Get FE User Information
$GLOBALS['TSFE']->fetch_the_id();
$GLOBALS['TSFE']->getPageAndRootline();
$GLOBALS['TSFE']->initTemplate();
$GLOBALS['TSFE']->tmpl->getFileName_backPath = PATH_site;
$GLOBALS['TSFE']->forceTemplateParsing = 1;
$GLOBALS['TSFE']->getConfigArray();
$GLOBALS['TSFE']->register['hello'] = 1;

/** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');

$respuesta = array(
    array(
        "name" => "Sprint 1",
        "desc" => "Tarea 1",
        "values" => [[
            "from" => "2016/02/02",
            "to" => "2016/02/28",
            "label" => "Tarea 1",
            "customClass" => "ganttRed",
            "dataObj"=> ["titulo" => 'Tarea 01', "descripcion" => 'Primera Tarea', "fecha inicio" => '2016/02/02', "fecha termino" => '2016/02/28' ]]]),
    array(
        "name" => "",
        "desc" => "Tarea 2",
        "values" => [[
            "from" => "2016/03/01",
            "to" => "2016/03/30",
            "label" => "Tarea 2",
            "customClass" => "ganttRed",
            "dataObj"=> ["titulo" => 'Tarea 02', "descripcion" => 'Segunda Tarea', "fecha inicio" => '2016/03/01', "fecha termino" => '2016/03/30' ]]]),
    array(
        "name" => "",
        "desc" => "Tarea 3",
        "values" => [[
            "from" => "2016/04/01",
            "to" => "2016/04/20",
            "label" => "Tarea 3",
            "customClass" => "ganttRed",
            "dataObj"=> ["titulo" => 'Tarea 03', "descripcion" => 'Tercera Tarea', "fecha inicio" => '2016/04/01', "fecha termino" => '2016/04/28' ]]]),
    array(
        "name" => "Sprint 2",
        "desc" => "Tarea 4",
        "values" => [[
            "from" => "2016/04/21",
            "to" => "2016/05/15",
            "label" => "Tarea 4",
            "customClass" => "ganttGreen",
            "dataObj"=> ["titulo" => 'Tarea 04', "descripcion" => 'Cuarta Tarea', "fecha inicio" => '2016/04/21', "fecha termino" => '2016/05/15' ]]]),
    array(
        "name" => "",
        "desc" => "Tarea 5",
        "values" => [[
            "from" => "2016/05/16",
            "to" => "2016/06/05",
            "label" => "Tarea 5",
            "customClass" => "ganttGreen",
            "dataObj"=> ["titulo" => 'Tarea 05', "descripcion" => 'Quinta Tarea', "fecha inicio" => '2016/05/16', "fecha termino" => '2016/06/05' ]]]),
    array(
        "name" => "",
        "desc" => "Tarea 6",
        "values" => [[
            "from" => "2016/06/06",
            "to" => "2016/06/30",
            "label" => "Tarea 6",
            "customClass" => "ganttGreen",
            "dataObj"=> ["titulo" => 'Tarea 06', "descripcion" => 'Sexta Tarea', "fecha inicio" => '2016/06/06', "fecha termino" => '2016/06/30' ]]]),
    array(
        "name" => "Sprint 3",
        "desc" => "Tarea 7",
        "values" => [[
            "from" => "2016/07/01",
            "to" => "2016/07/25",
            "label" => "Tarea 7",
            "customClass" => "ganttYellow",
            "dataObj"=> ["titulo" => 'Tarea 07', "descripcion" => 'Tarea 7', "fecha inicio" => '2016/07/01', "fecha termino" => '2016/07/25' ]]]),
    array(
        "name" => "",
        "desc" => "Tarea 8",
        "values" => [[
            "from" => "2016/07/26",
            "to" => "2016/08/15",
            "label" => "Tarea 8",
            "customClass" => "ganttYellow",
            "dataObj"=> ["titulo" => 'Tarea 08', "descripcion" => 'Tarea 88', "fecha inicio" => '2016/07/26', "fecha termino" => '2016/08/15' ]]]),
    array(
        "name" => "",
        "desc" => "Tarea 9",
        "values" => [[
            "from" => "2016/08/16",
            "to" => "2016/08/30",
            "label" => "Tarea 9",
            "customClass" => "ganttYellow",
            "dataObj"=> ["titulo" => 'Tarea 09', "descripcion" => 'Tarea 9999', "fecha inicio" => '2016/08/16', "fecha termino" => '2016/08/30' ]]]),
    array(
        "name" => "",
        "desc" => "Tarea 10",
        "values" => [[
            "from" => "2016/09/01",
            "to" => "2016/09/30",
            "label" => "Tarea 10",
            "customClass" => "ganttYellow",
            "dataObj"=> ["titulo" => 'Tarea 10', "descripcion" => 'Penultima Tarea', "fecha inicio" => '2016/09/01', "fecha termino" => '2016/09/30' ]]]),
    array(
    "name" => "",
    "desc" => "Tarea 11",
    "values" => [[
        "from" => "2016/10/01",
        "to" => "2016/12/30",
        "label" => "Tarea 11",
        "customClass" => "ganttYellow",
        "dataObj"=> ["titulo" => 'Tarea 11', "descripcion" => 'Tarea Final', "fecha inicio" => '2016/10/01', "fecha termino" => '2016/12/30' ]]]),
    array(
        "name" => "",
        "desc" => "Vacaciones",
        "values" => [[
            "from" => "2017/01/23",
            "to" => "2017/02/08",
            "label" => "Vacaciones",
            "customClass" => "ganttYellow",
            "dataObj"=> ["titulo" => 'Vacaciones', "descripcion" => 'Tarea Final', "fecha inicio" => '2016/10/01', "fecha termino" => '2016/12/30' ]]])
    );
echo json_encode($respuesta);